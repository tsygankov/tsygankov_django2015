import datetime
from django.contrib.auth.models import User
from django.db import models


class Message(models.Model):
    sender = models.ForeignKey(User)
    receiver = models.ForeignKey(User)
    time = models.DateTimeField(default=datetime.now)
    text = models.TextField()