from django import forms
from django.contrib.auth.models import User

from user_work.models import Profile


class AvatarForm(forms.ModelForm):
    class Meta:
        model = Profile
        fields =["avatar"]


class ProfileForm(forms.ModelForm):
    class Meta:
        model = Profile
        fields = ["avatar", "gender", "about"]

class UserForm(forms.ModelForm):
    class Meta:
        model = User
        fields = ["first_name", "last_name", "email"]