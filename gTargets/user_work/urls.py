from django.conf.urls import url

urlpatterns = [
    url(r'^$', 'user_work.views.index', name='current_user'),
    url(r'^(?P<user_id>\d+)$', 'user_work.views.user_profile', name='user_profile'),
    url(r'^load_avatar$', 'user_work.views.load_avatar', name='load_avatar'),
    url(r'^settings/profile$', 'user_work.views.edit_info', name='settings'),
    url(r'^settings/user$', 'user_work.views.edit_info_user', name='settings_user'),
]
