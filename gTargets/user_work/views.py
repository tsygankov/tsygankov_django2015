import os

from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User
from django.core.urlresolvers import reverse
from django.http import HttpResponseRedirect as Redirect
from django.shortcuts import render, get_object_or_404

from gTargets import settings
from user_work.forms import AvatarForm, ProfileForm, UserForm
from user_work.models import Profile


@login_required
def index(request):
    return Redirect(reverse('user:user_profile', kwargs={"user_id":request.user.id}))


@login_required
def user_profile(request, user_id):
    user = get_object_or_404(User, pk=user_id)
    profile = get_object_or_404(Profile, user=user)
    profile_current = Profile.objects.get(user=request.user)
    subscriptions = profile_current.subscriptions.all()
    subscribed = profile in subscriptions
    form = AvatarForm()
    return render(request, "user_work/user.html", {"form":form,
                                                   "current_user":request.user,
                                                   "subscribed":subscribed,
                                                   "user":{"user":user,
                                                           "profile":profile}})


@login_required
def load_avatar(request):
    if request.method == 'POST':
        profile = get_object_or_404(Profile, user_id=request.user.id)
        form = AvatarForm(request.POST, request.FILES, instance=profile)
        if form.is_valid:
            if profile.avatar:
                os.remove(os.path.join(settings.BASE_DIR, settings.MEDIA_ROOT,
                                       profile.avatar.name))
            form.save()
    return Redirect(reverse('user:current_user'))


@login_required
def edit_info(request):
    p = get_object_or_404(Profile, user=request.user)
    if request.method == "POST":
        form = ProfileForm(request.POST, request.FILES, instance=p)
        if form.is_valid():
            form.save()
            return Redirect(reverse("user:settings"))
        else:
            return render(request, "user_work/editinfo.html", {"form": form})
    else:
        form = ProfileForm(instance=p)
        return render(request, "user_work/editinfo.html", {"form": form})


@login_required
def edit_info_user(request):
    user = request.user
    if request.method == "POST":
        form = UserForm(request.POST, instance=user)
        if form.is_valid():
            form.save()
            return Redirect(reverse("user:settings_user"))
        else:
            return render(request, "user_work/editinfouser.html", {"form": form})
    else:
        form = UserForm(instance=user)
        return render(request, "user_work/editinfouser.html", {"form": form})
