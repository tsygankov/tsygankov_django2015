# -*- coding: utf-8 -*-
import os

from django.contrib.auth.models import User
from django.db import models


GENDER_CHOICES = ( #Почему не словарь?
        ('M', 'Male'),#а если пользователь русскоговорящий?
        ('F', 'Female'),
)


def get_path(instance, filename):
    return os.path.join('user', 'avatar', instance.user.username, filename)


class Profile(models.Model):
    user = models.OneToOneField(User, primary_key=True)
    about = models.TextField(blank=True)
    score = models.IntegerField(blank=True, default=0)
    rating = models.IntegerField(blank=True, default=0)
    gender = models.CharField(blank=True, max_length=1, choices=GENDER_CHOICES)
    subscriptions = models.ManyToManyField('Profile', blank=True)
    avatar = models.ImageField(blank=True, upload_to=get_path)

