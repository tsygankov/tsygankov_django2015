import os

from django.contrib.auth.models import User
from django.db import models


DIFFICULT = (
    (1, 'Very simple'),
    (2, 'Simple'),
    (3, 'Medium'),
    (4, 'Difficult'),
    (5, 'Very difficult'),
)


def get_path(instance, filename):
    return os.path.join('comments', instance.comment.pk, filename)


class TargetList(models.Model):
    user = models.ForeignKey(User)
    title = models.CharField(max_length=30)
    description = models.TextField()
    private = models.BooleanField(default=False)


class Theme(models.Model):
    title = models.CharField(max_length=30, primary_key=True)
    description = models.TextField()


class TargetTemplate(models.Model):
    theme = models.ForeignKey(Theme)
    title = models.CharField(max_length=30)
    description = models.TextField()


class Target(models.Model):
    target_list = models.ForeignKey(TargetList)
    theme = models.ForeignKey(Theme)
    title = models.CharField(max_length=30)
    description = models.TextField()
    everyday = models.BooleanField(default=False)
    date_started = models.DateTimeField()
    deadline_date = models.DateTimeField()
    difficult = models.IntegerField(choices=DIFFICULT)
    done = models.BooleanField(default=False)


class TargetComment(models.Model):
    user = models.ForeignKey(User)
    target = models.ForeignKey(Target)
    text = models.TextField()
    good_comment = models.BooleanField(default=False)


class Picture(models.Model):
    comment = models.ForeignKey(TargetComment)
    picture = models.ImageField(blank=False, upload_to=get_path)


class KeyWords(models.Model):
    target = models.ManyToManyField(Target)
    word = models.CharField(max_length=30)