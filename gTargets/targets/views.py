from django.contrib.auth.decorators import login_required
from django.http import HttpResponse
from django.shortcuts import render


@login_required
def index(request):
    profile = request.user.profile
    target_lists = profile.target_list_set.all()
    return HttpResponse(target_lists)
