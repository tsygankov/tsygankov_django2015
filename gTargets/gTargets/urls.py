"""gTargets URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.8/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Add an import:  from blog import urls as blog_urls
    2. Add a URL to urlpatterns:  url(r'^blog/', include(blog_urls))
"""
from django.conf.urls import include, url, patterns
from django.contrib import admin
from django.core.urlresolvers import reverse
from django.http import HttpResponseRedirect
from django.template.context_processors import static

from gTargets import settings


def redirect_to_user(request):
    return HttpResponseRedirect(reverse('user:current_user'))

urlpatterns = [
    url(r'^admin/', include(admin.site.urls)),
    url(r'^site/', include('sitework.urls', namespace='sitework')),
    url(r'^subscriptions/', include('subscriptions.urls', namespace='subscriptions')),
    url(r'^auth/', include('user_auth.urls', namespace='auth')),
    url(r'^user/', include('user_work.urls', namespace='user')),
    url(r'^$', redirect_to_user),
]

if settings.DEBUG:
    # static files (images, css, javascript, etc.)
    urlpatterns += patterns('',
        (r'^media/(?P<path>.*)$', 'django.views.static.serve', {
        'document_root': settings.MEDIA_ROOT}))
