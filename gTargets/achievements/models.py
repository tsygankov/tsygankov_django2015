from django.contrib.auth.models import User
from django.db import models


class Achievement(models.Model):
    name = models.CharField(max_length=30, primary_key=True)
    users = models.ManyToManyField(User)