from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.models import User
from django.core.urlresolvers import reverse
from django.db import IntegrityError
from django.http import HttpResponseRedirect as Redirect, HttpResponse, Http404
from django.shortcuts import render

# Create your views here.
from user_auth.forms import LoginForm
from user_work.models import Profile


def user_login(request):
    if request.method == 'POST':
        form = LoginForm(request.POST)
        errors = [];
        if form.is_valid():
            if form.cleaned_data["register"]:
                password_repeat = form.cleaned_data["password_repeat"]
                if password_repeat and password_repeat==form.cleaned_data["password"]:
                    try:
                        user = User(username=form.cleaned_data["username"])
                        user.set_password(password_repeat)
                        user.save()
                        profile = Profile(user_id=user.id)
                        profile.save()
                    except IntegrityError:
                        errors.append("Username isn't valid")
                        render(request, "user_auth/login.html", {"form": form,
                                                                 "errors": errors})
                else:
                    errors.append("Password are not equals")
                    return render(request, "user_auth/login.html", {"form": form,
                                                                    "errors": errors})
            user = authenticate(
                username=form.cleaned_data["username"],
                password=form.cleaned_data["password"]
            )

            if user is not None:
                login(request, user)
                if "next" in request.GET:
                    return Redirect(request.GET["next"])
                else:
                    return Redirect(reverse("user:current_user"))
            else:
                errors.append("User is not found or password is wrong")
                return render(request, "user_auth/login.html", {"form": form,
                    "errors": errors})
        else:
            errors.append("Form is not valid")
            return render(request, "user_auth/login.html", {"form": form,
                                                            "errors": errors})
    else:
        if request.user.is_anonymous():
            form = LoginForm()
            return render(request, 'user_auth/login.html', {"form": form})
        else:
            return Redirect(reverse("user:current_user"))


def user_logout(request):
    logout(request)
    return Redirect(reverse('auth:auth'))