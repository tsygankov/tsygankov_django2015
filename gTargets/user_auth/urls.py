from django.conf.urls import url

urlpatterns = [
    url(r'^$','user_auth.views.user_login', name='auth'),
    url(r'^logout$', 'user_auth.views.user_logout', name='logout'),
]
