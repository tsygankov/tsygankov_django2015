from django import forms


class LoginForm(forms.Form):
    username = forms.CharField()
    password = forms.CharField(widget=forms.PasswordInput)
    register = forms.BooleanField(initial=False, required=False)
    password_repeat = forms.CharField(widget=forms.PasswordInput, required=False)
