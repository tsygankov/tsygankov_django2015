from django.contrib import admin

from sitework.models import NewsComment, News

admin.site.register(News)
admin.site.register(NewsComment)

