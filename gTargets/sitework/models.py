from django.contrib.auth.models import User
from django.db import models


class News(models.Model):
    text = models.TextField()
    date = models.DateTimeField()


class NewsComment(models.Model):
    user = models.ForeignKey(User)
    news = models.ForeignKey(News)
    text = models.TextField()
    pub_date = models.DateTimeField()
    class Meta:
        ordering = ['pub_date']


class Suggestion(models.Model):
    user = models.ForeignKey(User)
    text = models.TextField()

