from django.conf.urls import url

from sitework.views import NewsView

urlpatterns = [
    url(r'^(?P<id>\d+)/$', 'sitework.views.get_news_item', name="news_item"),
    url(r'^$', NewsView.as_view(), name="news")
]
