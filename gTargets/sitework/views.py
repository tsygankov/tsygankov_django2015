from django.contrib.auth.decorators import login_required
from django.shortcuts import render
from django.views.generic import ListView

from sitework.models import News, NewsComment


def get_news_item(request, id):
    news = News.objects.get(id=id)
    comments = news.newscomment_set.all()
    return render(request, "sitework/news_item.html", {"news":news,
                                                       "newscomments":comments,
                                                       "user":request.user})


class NewsView(ListView):
    template_name = "sitework/news.html"
    model = News
    context_object_name = 'news_list'




