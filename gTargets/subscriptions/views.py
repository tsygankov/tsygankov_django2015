# -*- coding: utf-8 -*-
from django.contrib.auth.decorators import login_required
from django.core.urlresolvers import reverse
from django.http import HttpResponseRedirect as Redirect, HttpResponse
from django.shortcuts import render

from user_work.models import Profile


@login_required
def index(request):
    user = request.user
    profile = Profile.objects.get(user=user)
    subs = profile.subscriptions.all()
    return render(request, "subscriptions/subscriptions.html", {"profiles":subs,
                                                                "subscriptions":subs})


@login_required
def subscribe(request, user_id):
    if request.user.id != user_id:
        profile_current = Profile.objects.get(user_id=request.user.id)
        profile_to_subscribe = Profile.objects.get(user_id=user_id)
        profile_current.subscriptions.add(profile_to_subscribe)
    return Redirect(reverse("user:user_profile", kwargs={"user_id":user_id}))


@login_required
def subscribe_unsubscribe_ajax(request, user_id):
    profile_current = Profile.objects.get(user_id=request.user.id)
    if profile_current.subscriptions.filter(user_id=user_id).exists():
        unsubscribe(request, user_id)
        return HttpResponse("Подписка")
    else:
        subscribe(request, user_id)
        return HttpResponse("Отписка")



@login_required
def unsubscribe(request, user_id):
    if request.user.id != user_id:
        profile_current = Profile.objects.get(user_id=request.user.id)
        profile_to_subscribe = Profile.objects.get(user_id=user_id)
        profile_current.subscriptions.remove(profile_to_subscribe)
    return Redirect(reverse("user:user_profile", kwargs={"user_id":user_id}))


@login_required
def unsubscribe_ajax(request, user_id):
    subscribe(request, user_id)
    return HttpResponse("Subscribed")


@login_required
def user_subscriptions(request, user_id):
    profile = Profile.objects.get(user_id=user_id)
    subs = profile.subscriptions.all()
    current_user_subs = request.user.profile.subscriptions.all()
    return render(request, "subscriptions/subscriptions.html", {"profiles":subs,
                                                                "subscriptions":current_user_subs})
