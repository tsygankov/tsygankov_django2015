from django.conf.urls import url

urlpatterns = [
    url(r'^$', 'subscriptions.views.index', name="subscriptions"),
    url(r'^subscribe/(?P<user_id>\d+)$', 'subscriptions.views.subscribe', name="subscribe"),
    url(r'^unsubscribe/(?P<user_id>\d+)$', 'subscriptions.views.unsubscribe', name="unsubscribe"),
    url(r'^(?P<user_id>\d+)/$', 'subscriptions.views.user_subscriptions', name="user_subscriptions"),
    url(r'^ajax/subscribe/(?P<user_id>\d+)$', 'subscriptions.views.subscribe_unsubscribe_ajax', name="ajax_subs")
]