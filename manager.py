#-*-coding:utf-8-*-
import os
import shutil


def ls():
    for f in os.listdir("."):
        print f

def cat(args):
    if len(args) == 1:
        if os.path.isfile(args[0]):
            f = open(args[0])
            for line in f:
                print line.strip()
            f.close()
        else:
            print "Файл не найден"
    elif len(args) == 2 and args[0] == '>':
        if os.path.isfile(args[1]):
            f = open(args[1], 'w')
            text = raw_input()
            while text != ":q":
                f.write(text + "\n")
                text = raw_input()
            f.close()
        else:
            print "Файл не найден"
    else:
        print "Ошибка"

def cd(args):
    if len(args) == 1:
       os.chdir(args[0])
    else:
        print "Ошибка"

def touch(args):
    if len(args) == 1:
        if args[0] not in os.listdir("."):
            open(args[0], 'w').close()
        else:
            print "Файл уже существует"
    else:
        print "Ошибка"

def mkdir(args):
    if len(args) == 1:
        if args[0] not in os.listdir("."):
            os.mkdir(args[0])
        else:
            print "Каталог существует"
    else:
        print "Ошибка"

def rm(args):
    if len(args) == 1:
        if args[0] in os.listdir("."):
            ans = raw_input("Вы уверены, что хотите удалить %s (Y/N)? " % args[0])
            if ans == 'Y':
                if os.path.isfile(args[0]):
                    os.remove(args[0])
                elif os.path.isdir(args[0]):
                    os.rmdir(args[0])
            elif ans != 'N':
                print "Ошибка ввода"
        else:
            print "Путь не существует"
    else:
        print "Ошибка"

def cp(args):
    if len(args) == 2:
        if args[0] in os.listdir(".") and args[1] in os.listdir("."):
            if os.path.isfile(args[0]) and os.path.isfile(args[1]):
                shutil.copy(args[0], args[1])
        else:
            print "Путь не существует"
    else:
        print "Ошибка"

def make_command(cmd):
    try:
        splitted = cmd.split(" ")
        instruction = splitted[0]
        args = splitted[1:]
        if instruction == 'ls' or instruction == 'dir':
            ls()
        elif instruction == 'cat':
            cat(args)
        elif instruction == 'cd':
            cd(args)
        elif instruction == 'touch':
            touch(args)
        elif instruction == 'mkdir':
            mkdir(args)
        elif instruction == 'rm':
            rm(args)
        elif instruction == 'cp':
            cp(args)
        else:
            print "Команда неизвестна"
    except:
        print "Команда неправильна"
        
if __name__=='__main__':
    cmd = raw_input(os.path.abspath(os.curdir) + ">> ")
    while cmd != 'exit':
        make_command(cmd)
        cmd = raw_input(os.path.abspath(os.curdir) + ">> ")
        
        
    
